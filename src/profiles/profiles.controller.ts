import { Body, Controller, Param, Patch } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { Auth } from '../common/decorators/auth.decorator';
import { Permission } from '../common/decorators/permissions.decorator';
import { Permissions } from '../common/enum';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ProfilesService } from './profiles.service';
import { Profile } from './schemas/profile.schema';
@ApiTags('Profile')
@Controller('profiles')
export class ProfilesController {
  constructor(private readonly profilesService: ProfilesService) {}

  @Patch(':id')
  @Auth()
  @Permission(Permissions.UPDATE_PROFILE)
  update(
    @Param('id') _id: Types.ObjectId,
    @Body() updateProfileDto: UpdateProfileDto,
  ): Promise<Profile> {
    return this.profilesService.update(_id, updateProfileDto);
  }
}
