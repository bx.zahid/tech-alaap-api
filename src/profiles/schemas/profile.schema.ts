import { ModelOptions, plugin, prop, Ref } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import * as mongoosePopulate from 'mongoose-autopopulate';
import { AuthType } from '../../common/enum';
import { User } from '../../users/schemas/user.schema';

@plugin(mongoosePopulate as any)
@ModelOptions({ schemaOptions: { timestamps: true } })
export class Profile {
  public _id: Types.ObjectId;

  @prop({ enum: AuthType })
  public which?: string;

  @prop({ required: true, autopopulate: true, refPath: 'which' })
  public user: Ref<User>;

  @prop({ trim: true, default: '' })
  public firstName?: string;

  @prop({ trim: true, default: '' })
  public lastName?: string;

  @prop({ trim: true })
  public username?: string;

  @prop({ trim: true, default: '' })
  public profilePhoto?: string;

  @prop({ trim: true, default: '' })
  public phoneNumber?: string;

  @prop({ trim: true, default: '' })
  public designation?: string;

  @prop({ trim: true, default: '' })
  public location?: string;

  @prop({ trim: true, default: '' })
  public bio?: string;
}
