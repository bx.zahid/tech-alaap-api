import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken, TypegooseModule } from 'nestjs-typegoose';
import { ProfilesModule } from '../profiles/profiles.module';
import { Profile } from '../profiles/schemas/profile.schema';
import { SessionsModule } from '../sessions/sessions.module';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { User } from '../users/schemas/user.schema';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ProfilesService } from './profiles.service';

const config: ConfigService = new ConfigService();

describe('ProfilesService', () => {
  let userModel;
  let profileModel;

  let usersService: UsersService;
  let profilesService: ProfilesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypegooseModule.forFeature([User]),
        TypegooseModule.forFeature([Profile]),
        ConfigModule.forRoot(),
        TypegooseModule.forRoot(config.get<string>('DATABASE_URL'), {
          useCreateIndex: true,
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
        }),
        UsersModule,
        ProfilesModule,
        SessionsModule,
      ],
      providers: [UsersService],
    }).compile();

    userModel = module.get(getModelToken('User'));
    profileModel = module.get(getModelToken('Profile'));

    usersService = module.get<UsersService>(UsersService);
    profilesService = module.get<ProfilesService>(ProfilesService);

    await userModel.deleteMany({});
    await profileModel.deleteMany({});
  });

  afterEach(async () => {
    await userModel.deleteMany({});
    await profileModel.deleteMany({});
  });

  it('should be defined', () => {
    expect(profilesService).toBeDefined();
  });

  describe('update', () => {
    it('should be update profile and return that updated profile', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const fetchedUser = await usersService.create(createUserDto);

      const { _id } = await profilesService.findOneByUsername(
        fetchedUser.username,
      );

      const updateProfileDto: UpdateProfileDto = {
        firstName: 'Zahid',
        lastName: 'Hasan',
        username: 'bxzahid',
        profilePhoto: 'this-is-a-profile-photo',
        phoneNumber: '+8801521430199',
        designation: 'MERN Stack Developer',
        location: 'Mirpur - 2, Dhaka - 1216',
        bio:
          "Hi! I am Zahid Hasan. A passionate MERN Stack Developer. I studied B.Sc in CSE at Daffodil International University. I have matured knowledge in programming languages like C, JavaScript, TypeScript. I take a special interest in Back-end API design and development. Typically, I love to work with React.js, Node.js & GraphQL. I love to explore new technologies and tools. I'm flexible, proactive and curious.",
      };

      const {
        _id: id,
        firstName,
        lastName,
        username,
        profilePhoto,
        phoneNumber,
        designation,
        location,
        bio,
      } = await profilesService.update(_id, updateProfileDto);

      expect(id).toBeDefined();
      expect(firstName).toBe(updateProfileDto.firstName);
      expect(lastName).toBe(updateProfileDto.lastName);
      expect(username).toBe(updateProfileDto.username);
      expect(profilePhoto).toBe(updateProfileDto.profilePhoto);
      expect(phoneNumber).toBe(updateProfileDto.phoneNumber);
      expect(designation).toBe(updateProfileDto.designation);
      expect(location).toBe(updateProfileDto.location);
      expect(bio).toBe(updateProfileDto.bio);
    });
  });
});
