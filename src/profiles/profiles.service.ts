import { Injectable, NotFoundException } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { InjectModel } from 'nestjs-typegoose';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile } from './schemas/profile.schema';

@Injectable()
export class ProfilesService {
  constructor(
    @InjectModel(Profile)
    private readonly profileModel: ReturnModelType<typeof Profile>,
  ) {}

  async findOne(_id: Types.ObjectId): Promise<Profile> {
    return await this.profileModel.findById(_id);
  }

  async findOneByUsername(username: string): Promise<Profile> {
    return await this.profileModel.findOne({ username });
  }

  async update(
    _id: Types.ObjectId,
    updateProfileDto: UpdateProfileDto,
  ): Promise<Profile> {
    const profileExits: Profile = await this.findOne(_id);

    if (!profileExits)
      throw new NotFoundException('Profile id is not found or invalid');

    return await this.profileModel.findOneAndUpdate(
      { _id },
      { ...updateProfileDto },
      { new: true },
    );
  }
}
