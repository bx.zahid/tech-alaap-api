import { ApiProperty } from '@nestjs/swagger';
import { IsAlpha, IsString, Matches, MinLength } from 'class-validator';
import { Types } from 'mongoose';

export class UpdateProfileDto {
  @ApiProperty()
  _id?: Types.ObjectId;

  @ApiProperty()
  @IsAlpha()
  @IsString()
  @MinLength(3, { message: 'First name must be 3 character long' })
  firstName?: string;

  @ApiProperty()
  @IsAlpha()
  @IsString()
  @MinLength(3, { message: 'Last name must be 3 character long' })
  lastName?: string;

  @ApiProperty()
  @IsString()
  @Matches(/^(?=.{3,15}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/, {
    message:
      'Username is 3-15 characters long, No _ or . at the beginning, No __ or _. or ._ or .. inside, No _ or . at the end, Allowed characters: a-z A-Z 0-9',
  })
  username?: string;

  @ApiProperty()
  @IsString()
  profilePhoto?: string;

  @ApiProperty()
  @IsString()
  phoneNumber?: string;

  @ApiProperty()
  @IsString()
  @MinLength(3, { message: 'Designation must be 3 character long' })
  designation?: string;

  @ApiProperty()
  @IsString()
  @MinLength(4, { message: 'Location must be 4 character long' })
  location?: string;

  @ApiProperty()
  @IsString()
  @MinLength(8, { message: 'Location must be 8 character long' })
  bio?: string;
}
