import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthType } from 'src/common/enum';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { PermissionsGuard } from '../guards/permissions.guard';
import { RoleGuard } from '../guards/roles.guard';

export const Auth = (...roleTypes: AuthType[]) => {
  return applyDecorators(
    SetMetadata('roleTypes', roleTypes),
    UseGuards(new JwtAuthGuard(), RoleGuard, PermissionsGuard),
    // TODO: Not working when token pass from Swagger
    // ApiHeader({
    //   name: 'Authorization',
    // }),
    ApiBearerAuth(),
  );
};
