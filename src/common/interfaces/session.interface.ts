import { Request } from 'express';
import { Types } from 'mongoose';
import { AuthType } from '../enum';

export interface JwtPayload {
  iss: string;
  sub: Types.ObjectId;
  type: AuthType;
}

export interface SessionRequest extends Request {
  user: JwtPayload;
}

export interface GoogleSessionRequest extends Request {
  user: {
    googleSchemaId: string;
    email: string;
    firstName: string;
    lastName: string;
    profilePhoto: string;
    isVerified: boolean;
  };
}

export interface FacebookSessionRequest extends Request {
  user: {
    facebookSchemaId: string;
    email: string;
    firstName: string;
    lastName: string;
    profilePhoto: string;
  };
}

export interface GithubSessionRequest extends Request {
  user: {
    githubSchemaId: string;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    profilePhoto: string;
    location: string;
    bio: string;
  };
}
