export interface CreateGoogleUser {
  googleSchemaId: string;
  email: string;
  firstName: string;
  lastName: string;
  profilePhoto: string;
  isVerified: boolean;
}

export interface CreateFacebookUser {
  facebookSchemaId: string;
  email: string;
  firstName: string;
  lastName: string;
  profilePhoto: string;
}

export interface CreateGithubUser {
  githubSchemaId: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  profilePhoto: string;
  location: string;
  bio: string;
}
