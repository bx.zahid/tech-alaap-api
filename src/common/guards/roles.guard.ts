import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthType } from '../enum';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roleTypes: string[] = this.reflector.get<AuthType[]>(
      'roleTypes',
      context.getHandler(),
    );

    const request = context.switchToHttp().getRequest();

    if (!roleTypes?.length) return true;

    const hasRole: boolean = roleTypes.includes(request?.user.sessionExists.type);

    if (hasRole) {
      return true;
    } else {
      throw new ForbiddenException('Your permission is denied');
    }
  }
}
