import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const routePermissions = this.reflector.get<string[]>(
      'permissions',
      context.getHandler(),
    );

    if (!routePermissions) return true;

    const request = context.switchToHttp().getRequest();

    if (request?.user?.sessionExists.type === 'ADMIN') return true;

    const userPermissions = request?.user?.permissions;

    const hasPermission: boolean = userPermissions.includes(routePermissions);

    if (hasPermission) {
      return true;
    } else {
      throw new ForbiddenException(
        "You don't have permission to access this route",
      );
    }
  }
}
