import { MailerModule } from '@nestjs-modules/mailer';
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { join } from 'path';
import { EmailService } from './email.service';

const config: ConfigService = new ConfigService();

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MailerModule.forRoot({
      transport: {
        host: config.get('MAIL_HOST'),
        port: config.get('MAIL_PORT'),
        ignoreTLS: true,
        secure: false,
        auth: {
          user: config.get('MAIL_USER'),
          pass: config.get('MAIL_PASSWORD'),
        },
      },
      defaults: {
        from: `${config.get('APP_NAME')} <${config.get('MAIL_FROM')}>`,
      },
      template: {
        dir: join(__dirname, 'templates'),
        adapter: new EjsAdapter({ inlineCssEnabled: true }),
        options: {
          strict: true,
        },
      },
    }),
    EmailModule,
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
