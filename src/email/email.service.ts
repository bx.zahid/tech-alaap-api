import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class EmailService {
  constructor(private readonly mailerService: MailerService) {}

  async sendSignupMail(): Promise<boolean> {
    try {
      await this.mailerService.sendMail({
        to: 'test@test.com',
        subject: 'Tech Alaap',
        template: 'sendSignupMail',
        context: {
          name: 'Jodu Modu Codu',
        },
        html: 'Hello from sendSignupMail',
      });
      return true;
    } catch {
      return false;
    }
  }

  async sendPasswordResetMail(template: string): Promise<boolean> {
    try {
      await this.mailerService.sendMail({
        to: 'test@test.com',
        subject: 'Tech Alaap',
        html: template,
      });
      return true;
    } catch {
      return false;
    }
  }
}
