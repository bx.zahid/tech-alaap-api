import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DocumentType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { Auth } from '../common/decorators/auth.decorator';
import { AuthType, Permissions } from '../common/enum';
import { Permission } from './../common/decorators/permissions.decorator';
import { CreateUserPasswordDto } from './dto/create-user-password.dto';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schemas/user.schema';
import { UsersService } from './users.service';

@ApiTags('User')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('create-password/:id')
  @Auth()
  @Permission(Permissions.CREATE_PASSWORD)
  createPassword(
    @Param('id') id: Types.ObjectId,
    @Body() createUserPasswordDto: CreateUserPasswordDto,
  ): Promise<string> {
    return this.usersService.createPassword(id, createUserPasswordDto);
  }

  @Get()
  @Auth(AuthType.ADMIN)
  findAll(): Promise<DocumentType<User>[]> {
    return this.usersService.findAll();
  }

  @Get(':id')
  @Auth(AuthType.ADMIN)
  findOne(@Param('id') id: Types.ObjectId): Promise<User> {
    return this.usersService.findOne(id);
  }

  @Patch(':id')
  @Auth()
  @Permission(Permissions.UPDATE)
  update(
    @Param('id') _id: Types.ObjectId,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.usersService.update(_id, updateUserDto);
  }

  @Patch('update-password/:id')
  @Auth()
  @Permission(Permissions.UPDATE_PASSWORD)
  updatePassword(
    @Param('id') id: Types.ObjectId,
    @Body() updateUserPasswordDto: UpdateUserPasswordDto,
  ): Promise<string> {
    return this.usersService.updatePassword(id, updateUserPasswordDto);
  }

  @Delete(':id')
  @Auth(AuthType.ADMIN)
  remove(@Param('id') _id: Types.ObjectId): Promise<User> {
    return this.usersService.remove(_id);
  }
}
