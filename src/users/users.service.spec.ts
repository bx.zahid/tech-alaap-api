import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken, TypegooseModule } from 'nestjs-typegoose';
import { AuthModule } from '../auth/auth.module';
import { ProfilesModule } from '../profiles/profiles.module';
import { Profile } from '../profiles/schemas/profile.schema';
import { SessionsModule } from '../sessions/sessions.module';
import { UsersModule } from '../users/users.module';
import { CreateUserPasswordDto } from './dto/create-user-password.dto';
import { CreateUserDto } from './dto/create-User.dto';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schemas/User.schema';
import { UsersService } from './Users.service';

const config: ConfigService = new ConfigService();

describe('UsersService', () => {
  let userModel;
  let profileModel;

  let usersService: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypegooseModule.forFeature([User]),
        TypegooseModule.forFeature([Profile]),
        ConfigModule.forRoot(),
        TypegooseModule.forRoot(config.get<string>('DATABASE_URL'), {
          useCreateIndex: true,
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
        }),
        UsersModule,
        ProfilesModule,
        AuthModule,
        SessionsModule,
      ],
      providers: [UsersService],
    }).compile();

    userModel = module.get(getModelToken('User'));
    profileModel = module.get(getModelToken('Profile'));

    usersService = module.get<UsersService>(UsersService);

    await userModel.deleteMany({});
    await profileModel.deleteMany({});
  });

  afterEach(async () => {
    await userModel.deleteMany({});
    await profileModel.deleteMany({});
  });

  it('should be defined', () => {
    expect(usersService).toBeDefined();
  });

  describe('create', () => {
    it('should be create a user and return that user', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id, username, email, password } = await usersService.create(
        createUserDto,
      );

      expect(_id).toBeDefined();
      expect(username).toBe(createUserDto.username);
      expect(email).toBe(createUserDto.email);
      expect(password).toBeDefined();
    });

    it('should throw error when username or email is not unique', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await usersService.create(createUserDto);

      try {
        await usersService.create(createUserDto);
      } catch (error) {
        expect(error.name).toBe('ValidationError');
      }
    });
  });

  describe('createPassword', () => {
    it('should be update user password and return a string', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await usersService.create(createUserDto);

      await usersService.update(_id, { password: undefined });

      const createUserPasswordDto: CreateUserPasswordDto = {
        password: 'Zahid123456',
      };

      const response = await usersService.createPassword(
        _id,
        createUserPasswordDto,
      );

      expect('Password addedd successfull').toBe(response);
    });
  });

  // FIXME:
  // describe('findAll', () => {
  //   it('should be return an array of user', async () => {
  //     const createUserDto: CreateUserDto = {
  //       username: 'bxzahid',
  //       email: 'zahidhasan.dev@gmail.com',
  //       password: 'Zahid123456',
  //     };

  //     await usersService.create(createUserDto);

  //     const fetchedUser: DocumentType<User>[] = await usersService.findAll();

  //     expect(fetchedUser._id).toBeDefined();
  //     expect(fetchedUser.username).toBe(createUserDto.username);
  //     expect(fetchedUser.email).toBe(createUserDto.email);
  //     expect(fetchedUser.password).toBeDefined();
  //     expect(fetchedUser.authType).toEqual('User');
  //   });
  // });

  describe('findOne', () => {
    it('should be return an user by _id', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await usersService.create(createUserDto);

      const { _id: id, username, email, password } = await usersService.findOne(
        _id,
      );

      expect(id).toBeDefined();
      expect(username).toBe(createUserDto.username);
      expect(email).toBe(createUserDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('findOneByEmail', () => {
    it('should be return an user by email', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { email } = await usersService.create(createUserDto);

      const {
        _id,
        username,
        email: userEmail,
        password,
      } = await usersService.findOneByEmail(email);

      expect(_id).toBeDefined();
      expect(username).toBe(createUserDto.username);
      expect(userEmail).toBe(createUserDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('findOneByIdentifier', () => {
    it('should be return an user by username or email', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await usersService.create(createUserDto);

      const {
        _id,
        username,
        email,
        password,
      } = await usersService.findOneByIdentifier(createUserDto.username);

      expect(_id).toBeDefined();
      expect(username).toBe(createUserDto.username);
      expect(email).toBe(createUserDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('update', () => {
    it('should be update user and return that updated user', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await usersService.create(createUserDto);

      const updateUserDto: UpdateUserDto = {
        username: 'updated username',
        email: 'updated.dev@gmail.com',
        password: 'Updated123456',
      };

      const { _id: id, username, email, password } = await usersService.update(
        _id,
        updateUserDto,
      );

      expect(id).toBeDefined();
      expect(username).toBe(updateUserDto.username);
      expect(email).toBe(updateUserDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('updatePassword', () => {
    it('should be update user password and return a string', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await usersService.create(createUserDto);

      const updateUserPasswordDto: UpdateUserPasswordDto = {
        currentPassword: 'Zahid123456',
        newPassword: 'Updated123456',
      };

      const response = await usersService.updatePassword(
        _id,
        updateUserPasswordDto,
      );

      expect('Password change successfully').toBe(response);
    });
  });

  describe('remove', () => {
    it('should be remove user by _id and return that removed user', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await usersService.create(createUserDto);

      const { _id: id, username, email, password } = await usersService.remove(
        _id,
      );

      expect(id).toBeDefined();
      expect(username).toBe(createUserDto.username);
      expect(email).toBe(createUserDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('count', () => {
    it('should be return total number of user', async () => {
      const createUserDto: CreateUserDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await usersService.create(createUserDto);

      const response = await usersService.count();

      expect(response).toBe(1);
    });
  });
});
