import { ModelOptions, plugin, pre, prop, Ref } from '@typegoose/typegoose';
import { compareSync, hashSync } from 'bcryptjs';
import { Types } from 'mongoose';
import * as mongoosePopulate from 'mongoose-autopopulate';
import * as uniqueValidator from 'mongoose-unique-validator';
import { AuthType, Provider } from '../../common/enum';
import { Profile } from '../../profiles/schemas/profile.schema';

class Role {
  @prop({ required: true, default: AuthType.USER })
  public name: string;

  @prop({ required: true, type: [String] })
  public permissions: string[];
}

@plugin(uniqueValidator, { message: '{VALUE} already taken' })
@plugin(mongoosePopulate as any)
@ModelOptions({ schemaOptions: { timestamps: true } })
@pre<User>('save', function () {
  this.password = hashSync(this.password);
})
export class User {
  public _id?: Types.ObjectId;

  @prop({ trim: true, unique: true })
  public username?: string;

  @prop({ trim: true, lowercase: true, unique: true })
  public email: string;

  @prop({ trim: true })
  public password?: string;

  @prop()
  public googleSchemaId?: string;

  @prop()
  public facebookSchemaId?: string;

  @prop()
  public twitterSchemaId?: string;

  @prop()
  public githubSchemaId?: string;

  @prop({ autopopulate: true, ref: 'Profile' })
  public profile?: Ref<Profile>;

  @prop({ default: AuthType.USER })
  public authType?: AuthType;

  @prop({ enum: Provider })
  public provider?: Provider;

  @prop({ type: () => Role })
  public role?: Role;

  @prop()
  public articles?: unknown;

  @prop()
  public questions?: unknown;

  @prop()
  public bookmarksArticle?: unknown;

  @prop()
  public bookmarksQuestion?: unknown;

  @prop()
  public slider?: unknown;

  @prop({ type: Date })
  public passwordChangedAt?: string;

  @prop()
  public passwordResetToken?: string;

  @prop({ default: false })
  public isVerified?: boolean;

  @prop({ default: false })
  public isBlock?: boolean;

  public comparePassword(currentPassword: string): boolean {
    return compareSync(currentPassword, this.password);
  }
}
