import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Profile } from '../profiles/schemas/profile.schema';
import { Role } from '../roles/schemas/roles.schema';
import { SessionsModule } from '../sessions/sessions.module';
import { User } from './schemas/user.schema';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  imports: [
    TypegooseModule.forFeature([User]),
    TypegooseModule.forFeature([Profile]),
    TypegooseModule.forFeature([Role]),
    SessionsModule,
  ],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}
