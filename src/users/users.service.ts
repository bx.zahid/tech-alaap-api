import {
  ForbiddenException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { DocumentType, ReturnModelType } from '@typegoose/typegoose';
import { hashSync } from 'bcryptjs';
import { Types } from 'mongoose';
import { InjectModel } from 'nestjs-typegoose';
import {
  CreateFacebookUser,
  CreateGithubUser,
  CreateGoogleUser
} from 'src/common/interfaces/provider.interface';
import { SessionRequest } from 'src/common/interfaces/session.interface';
import { AuthType, Permissions, Provider } from '../common/enum';
import { Profile } from '../profiles/schemas/profile.schema';
import { SessionsService } from '../sessions/sessions.service';
import { CreateUserPasswordDto } from './dto/create-user-password.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User)
    private readonly userModel: ReturnModelType<typeof User>,

    @InjectModel(Profile)
    private readonly profileModel: ReturnModelType<typeof Profile>,

    private readonly sessionsService: SessionsService,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const users: User = await this.userModel.create(createUserDto);
    const profiles: Profile = await this.profileModel.create({
      user: users._id,
      username: users.username,
    });

    await this.update(users._id, {
      profile: profiles._id,
      role: {
        name: AuthType.USER,
        permissions: [...Object.values(Permissions)],
      },
    });

    return users;
  }

  async createGoogleUser(
    createGoogleUserData: CreateGoogleUser,
  ): Promise<User> {
    const {
      googleSchemaId,
      email,
      firstName,
      lastName,
      profilePhoto,
      isVerified,
    } = createGoogleUserData;

    const users: User = await this.userModel.create({
      googleSchemaId,
      email,
      provider: Provider.GOOGLE,
      isVerified,
    });

    const profiles: Profile = await this.profileModel.create({
      user: users._id,
      firstName,
      lastName,
      profilePhoto,
    });

    await this.update(users._id, {
      profile: profiles._id,
      role: {
        name: AuthType.USER,
        permissions: [...Object.values(Permissions)],
      },
    });

    return users;
  }

  async createFacebookUser(
    createFacebookUserData: CreateFacebookUser,
  ): Promise<User> {
    const {
      facebookSchemaId,
      email,
      firstName,
      lastName,
      profilePhoto,
    } = createFacebookUserData;

    const users: User = await this.userModel.create({
      facebookSchemaId,
      email,
      provider: Provider.FACEBOOK,
    });

    const profiles: Profile = await this.profileModel.create({
      user: users._id,
      firstName,
      lastName,
      profilePhoto,
    });

    await this.update(users._id, {
      profile: profiles._id,
      role: {
        name: AuthType.USER,
        permissions: [...Object.values(Permissions)],
      },
    });

    return users;
  }

  async createGithubUser(
    createGithubUserData: CreateGithubUser,
  ): Promise<User> {
    const {
      githubSchemaId,
      username,
      email,
      firstName,
      lastName,
      profilePhoto,
      location,
      bio,
    } = createGithubUserData;

    const users: User = await this.userModel.create({
      githubSchemaId,
      username,
      email,
      provider: Provider.GITHUB,
    });

    const profiles: Profile = await this.profileModel.create({
      user: users._id,
      firstName,
      lastName,
      profilePhoto,
      location,
      bio,
    });

    await this.update(users._id, {
      profile: profiles._id,
      role: {
        name: AuthType.USER,
        permissions: [...Object.values(Permissions)],
      },
    });

    return users;
  }

  async createPassword(
    _id: Types.ObjectId,
    createUserPasswordDto: CreateUserPasswordDto,
  ): Promise<string> {
    const user: User = await this.findOne(_id);

    if (user.password) return 'Password already exits';

    const hashPassword: string = hashSync(createUserPasswordDto.password);

    const updated: User = await this.userModel.updateOne(
      { _id },
      { password: hashPassword },
    );
    if (updated) return 'Password addedd successfull';
  }

  async findAll(): Promise<DocumentType<User>[]> {
    return await this.userModel.find();
  }

  async findOne(_id: Types.ObjectId): Promise<DocumentType<User>> {
    return await this.userModel.findById(_id);
  }

  async findOneByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email });
  }

  async findOneByIdentifier(identifier: string): Promise<User> {
    return await this.userModel.findOne({
      $or: [{ username: identifier }, { email: identifier }],
    });
  }

  async findOneByGoogleSchemaId(
    googleSchemaId: string,
  ): Promise<DocumentType<User>> {
    return await this.userModel.findOne({ googleSchemaId });
  }

  async findOneByFacebookSchemaId(
    facebookSchemaId: string,
  ): Promise<DocumentType<User>> {
    return await this.userModel.findOne({ facebookSchemaId });
  }

  async findOneByGithubSchemaId(
    githubSchemaId: string,
  ): Promise<DocumentType<User>> {
    return await this.userModel.findOne({ githubSchemaId });
  }

  async update(
    _id: Types.ObjectId,
    updateUserDto: UpdateUserDto,
  ): Promise<DocumentType<User>> {
    const userExits: User = await this.findOne(_id);

    if (!userExits)
      throw new NotFoundException('User id is not found or invalid');

    return await this.userModel.findOneAndUpdate(
      { _id },
      { ...updateUserDto },
      { new: true },
    );
  }

  async updatePassword(
    _id: Types.ObjectId,
    updateUserPasswordDto: UpdateUserPasswordDto,
  ): Promise<string> {
    const user: User = await this.findOne(_id);

    const matched: boolean = user.comparePassword(
      updateUserPasswordDto.currentPassword,
    );

    if (!matched) throw new NotFoundException('Your current password is wrong');

    const newHashPassword: string = hashSync(updateUserPasswordDto.newPassword);

    const updated: User = await this.userModel.updateOne(
      { _id },
      { password: newHashPassword },
    );
    if (updated) return 'Password change successfully';
  }

  async remove(_id: Types.ObjectId): Promise<DocumentType<User>> {
    return await this.userModel.findByIdAndDelete(_id);
  }

  // TODO: Have to add it to spec
  async logout(req: SessionRequest): Promise<string> {
    if (this.sessionsService.remove(req)) {
      return 'You have logged out successfully';
    } else {
      throw new ForbiddenException(
        'Invalid token or you have already been logged out',
      );
    }
  }

  async count(): Promise<number> {
    return await this.userModel.countDocuments();
  }
}
