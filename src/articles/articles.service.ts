import { Injectable, NotFoundException } from '@nestjs/common';
import { DocumentType, ReturnModelType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { InjectModel } from 'nestjs-typegoose';
import { AuthType } from './../common/enum';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article } from './schemas/articles.schema';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article)
    private readonly articleModel: ReturnModelType<typeof Article>,
  ) {}

  async create(
    authorId: Types.ObjectId,
    createArticleDto: CreateArticleDto,
  ): Promise<Article> {
    const article: Article = await this.articleModel.create({
      author: authorId,
      ...createArticleDto,
    });

    return article;
  }

  async findAll(): Promise<DocumentType<Article>[]> {
    return await this.articleModel.find();
  }

  async findOne(_id: Types.ObjectId): Promise<DocumentType<Article>> {
    return await this.articleModel.findById(_id);
  }

  async findOneByIdOrSlug(_id: Types.ObjectId, slug: string): Promise<Article> {
    return await this.articleModel.findOne({
      $or: [{ _id }, { slug }],
    });
  }

  async findOneByTag(tag): Promise<DocumentType<Article>[]> {
    return await this.articleModel.find({
      tags: { $in: [tag] },
    });
  }

  async update(
    _id: Types.ObjectId,
    authorId: Types.ObjectId,
    authType: AuthType,
    updateArticleDto: UpdateArticleDto,
  ): Promise<DocumentType<Article>> {
    const articleExits: Article = await this.findOne(_id);

    if (!articleExits) throw new NotFoundException('Article is not found');

    // FIXME:
    // if (
    //   !(authType === AuthType.ADMIN || articleExits.author._id.equals(authorId))
    // ) {
    //   throw new ForbiddenException('This article is not your');
    // }

    return await this.articleModel.findOneAndUpdate(
      { _id },
      { ...updateArticleDto },
      { new: true },
    );
  }

  async remove(
    _id: Types.ObjectId,
    authorId: Types.ObjectId,
    authType: AuthType,
  ): Promise<DocumentType<Article>> {
    const articleExits = await this.findOne(_id);

    if (!articleExits) throw new NotFoundException('Article is not found');

    // FIXME:
    // if (
    //   !(authType === AuthType.ADMIN || articleExits.author._id.equals(authorId))
    // ) {
    //   throw new ForbiddenException('This article is not your');
    // }

    return await this.articleModel.findByIdAndDelete(_id);
  }

  async count(): Promise<number> {
    return await this.articleModel.countDocuments();
  }
}
