import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { Article } from './schemas/articles.schema';

@Module({
  imports: [TypegooseModule.forFeature([Article])],
  providers: [ArticlesService],
  controllers: [ArticlesController],
})
export class ArticlesModule {}
