import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, MinLength } from 'class-validator';
import { Types } from 'mongoose';

export class UpdateArticleDto {
  _id?: Types.ObjectId;

  @ApiProperty()
  @IsString({})
  @MinLength(4, { message: 'Title must be 4 character long' })
  @MaxLength(120, { message: 'Title should be at most 120 character long' })
  title?: string;

  @ApiProperty()
  @IsString()
  @MinLength(50, { message: 'Title must be 50 character long' })
  @MaxLength(2500, { message: 'Title should be at most 2500 character long' })
  body?: string;

  @ApiProperty()
  @IsString()
  thumbnail?: string;

  @ApiProperty()
  tags?: string[];
}
