import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { Types } from 'mongoose';

export class CreateArticleDto {
  _id?: Types.ObjectId;

  @ApiProperty()
  @IsString()
  @IsNotEmpty({ message: 'Title should not be empty' })
  @MinLength(4, { message: 'Title must be 4 character long' })
  @MaxLength(120, { message: 'Title should be at most 120 character long' })
  title: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty({ message: 'Body should not be empty' })
  @MinLength(50, { message: 'Body must be 50 character long' })
  @MaxLength(2500, { message: 'Body should be at most 2500 character long' })
  body: string;

  @ApiProperty()
  @IsString()
  thumbnail: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'Tags should not be empty' })
  tags: string[];
}
