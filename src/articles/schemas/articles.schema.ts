import { plugin, prop, Ref } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import * as mongoosePopulate from 'mongoose-autopopulate';
import { User } from 'src/users/schemas/user.schema';

@plugin(mongoosePopulate as any)
export class Article {
  public _id: Types.ObjectId;

  @prop({ autopopulate: true, ref: 'User' })
  public author?: Ref<User>;

  @prop({ trim: true })
  public title: string;

  @prop({ trim: true, lowercase: true })
  public slug?: string;

  public excerpt?: string;

  @prop({ trim: true })
  public body: string;

  @prop({ trim: true })
  public thumbnail: string;

  public timeToRead?: number;

  @prop({ default: false })
  public isPublished?: boolean;

  @prop({ default: false })
  public isBookmarked?: boolean;

  @prop({ default: false })
  public isFeatured?: boolean;

  @prop()
  public comments?: unknown;

  @prop()
  public categories?: unknown;

  @prop({ trim: true })
  public tags: string[];
}
