import {
  Body,
  Controller,
  Delete,
  Param,
  Patch,
  Post,
  Request,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DocumentType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { Auth } from 'src/common/decorators/auth.decorator';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article } from './schemas/articles.schema';

@ApiTags('Article')
@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Post()
  @Auth()
  create(
    @Body() createArticleDto: CreateArticleDto,
    @Request() req: any,
  ): Promise<Article> {
    return this.articlesService.create(
      req.user.sessionExists._id,
      createArticleDto,
    );
  }

  @Patch(':id')
  @Auth()
  update(
    @Param('id') _id: Types.ObjectId,
    @Request() req: any,
    @Body() updateArticleDto: UpdateArticleDto,
  ): Promise<DocumentType<Article>> {
    return this.articlesService.update(
      _id,
      req.user.sessionExists._id,
      req.user.sessionExists.type,
      updateArticleDto,
    );
  }

  @Delete(':id')
  @Auth()
  remove(
    @Param('id') _id: Types.ObjectId,
    @Request() req: any,
  ): Promise<DocumentType<Article>> {
    return this.articlesService.remove(
      _id,
      req.user.sessionExists._id,
      req.user.sessionExists.type,
    );
  }
}
