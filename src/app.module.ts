import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypegooseModule } from 'nestjs-typegoose';
import { AdminsModule } from './admins/admins.module';
import { Admin } from './admins/schemas/admin.schema';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { Article } from './articles/schemas/articles.schema';
import { AuthModule } from './auth/auth.module';
import { ProfilesModule } from './profiles/profiles.module';
import { Profile } from './profiles/schemas/profile.schema';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/schemas/roles.schema';
import { Session } from './sessions/schemas/session.schema';
import { SessionsModule } from './sessions/sessions.module';
import { User } from './users/schemas/user.schema';
import { UsersModule } from './users/users.module';

const config: ConfigService = new ConfigService();

@Module({
  imports: [
    TypegooseModule.forFeature([User]),
    TypegooseModule.forFeature([Profile]),
    TypegooseModule.forFeature([Article]),
    TypegooseModule.forFeature([Admin]),
    TypegooseModule.forFeature([Role]),
    TypegooseModule.forFeature([Session]),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypegooseModule.forRoot(config.get<string>('DATABASE_URL'), {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    }),
    UsersModule,
    SessionsModule,
    AuthModule,
    AdminsModule,
    ProfilesModule,
    RolesModule,
    ArticlesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
