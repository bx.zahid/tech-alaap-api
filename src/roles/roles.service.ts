import { Injectable, NotFoundException } from '@nestjs/common';
import { DocumentType, ReturnModelType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { InjectModel } from 'nestjs-typegoose';
import { AuthType, Permissions } from './../common/enum';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './schemas/roles.schema';

@Injectable()
export class RolesService {
  constructor(
    @InjectModel(Role)
    private readonly roleModel: ReturnModelType<typeof Role>,
  ) {}

  async create(createRoleDto: CreateRoleDto): Promise<DocumentType<Role>> {
    return await this.roleModel.create(createRoleDto);
  }

  async createDefaultRole(): Promise<DocumentType<Role>> {
    return await this.roleModel.create({
      name: AuthType.USER,
      permissions: [...Object.values(Permissions)],
    });
  }

  async findAll(): Promise<DocumentType<Role>[]> {
    return await this.roleModel.find();
  }

  async findOne(_id: Types.ObjectId): Promise<DocumentType<Role>> {
    return await this.roleModel.findById(_id);
  }

  async findOneByName(name: string): Promise<DocumentType<Role>> {
    return await this.roleModel.findOne({ name });
  }

  async update(
    _id: Types.ObjectId,
    updateRoleDto: UpdateRoleDto,
  ): Promise<DocumentType<Role>> {
    const roleExits: Role = await this.findOne(_id);

    if (!roleExits) throw new NotFoundException('Role not found');

    return await this.roleModel.findOneAndUpdate(
      { _id },
      { ...updateRoleDto },
      { new: true },
    );
  }

  async remove(_id: Types.ObjectId): Promise<any> {
    const roles: Role = await this.roleModel.findByIdAndRemove(_id);

    if (roles)
      return {
        message: 'Role deleted successfully',
      };
  }
}
