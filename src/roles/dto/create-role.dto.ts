import { ApiProperty } from '@nestjs/swagger';
import { IsAlpha, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { Types } from 'mongoose';
import { Permissions } from 'src/common/enum';

export class CreateRoleDto {
  _id?: Types.ObjectId;

  @ApiProperty()
  @IsNotEmpty()
  @IsAlpha()
  @IsString()
  @MinLength(3, { message: 'Role name must be 3 character long' })
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  permissions: Permissions[];
}
