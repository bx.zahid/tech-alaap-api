import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DocumentType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { Auth } from '../common/decorators/auth.decorator';
import { AuthType } from '../common/enum';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { RolesService } from './roles.service';
import { Role } from './schemas/roles.schema';

@ApiTags('Role')
@Controller('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @Post()
  @Auth(AuthType.ADMIN)
  async create(
    @Body() createRoleDto: CreateRoleDto,
  ): Promise<DocumentType<Role>> {
    return this.rolesService.create(createRoleDto);
  }

  @Get()
  @Auth(AuthType.ADMIN)
  async findAll(): Promise<DocumentType<Role>[]> {
    return this.rolesService.findAll();
  }

  @Patch(':id')
  @Auth(AuthType.ADMIN)
  update(
    @Param('id') _id: Types.ObjectId,
    @Body() updateRoleDto: UpdateRoleDto,
  ): Promise<DocumentType<Role>> {
    return this.rolesService.update(_id, updateRoleDto);
  }

  @Delete(':id')
  @Auth(AuthType.ADMIN)
  remove(@Param('id') _id: Types.ObjectId): Promise<Role> {
    return this.rolesService.remove(_id);
  }
}
