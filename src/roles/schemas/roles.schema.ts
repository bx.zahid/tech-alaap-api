import { ModelOptions, plugin, prop } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import * as uniqueValidator from 'mongoose-unique-validator';

@plugin(uniqueValidator, { message: '{VALUE} already taken' })
@ModelOptions({ schemaOptions: { timestamps: true } })
export class Role {
  public _id?: Types.ObjectId;

  @prop({ trim: true, unique: true })
  public name?: string;

  @prop({
    required: true,
    type: [String],
  })
  public permissions?: string[];
}
