import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { RolesController } from './roles.controller';
import { RolesService } from './roles.service';
import { Role } from './schemas/roles.schema';

@Module({
  imports: [TypegooseModule.forFeature([Role])],
  providers: [RolesService],
  controllers: [RolesController],
  exports: [RolesService],
})
export class RolesModule {}
