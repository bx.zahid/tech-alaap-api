import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-github2';

@Injectable()
export class GithubStrategy extends PassportStrategy(Strategy, 'github') {
  constructor(configService: ConfigService) {
    super({
      clientID: configService.get<string>('GITHUB_CLIENT_ID'),
      clientSecret: configService.get<string>('GITHUB_CLIENT_SECRET'),
      callbackURL: configService.get<string>('GITHUB_CALLBACK_URL'),
      scope: ['public_profile', 'email'],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    cb: (err: any, user: any, info?: any) => void,
  ): Promise<void> {
    const {
      _json: { id, avatar_url, name, location, email, bio, twitter_username },
    } = profile;

    const payload = {
      githubSchemaId: id,
      username: twitter_username,
      email: email,
      firstName: name.split(' ')[0],
      lastName: name.split(' ')[1],
      profilePhoto: avatar_url,
      location,
      bio,
    };

    cb(null, payload);
  }
}
