import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from '../../common/interfaces/session.interface';
import { RolesService } from '../../roles/roles.service';
import { SessionsService } from '../../sessions/sessions.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    configService: ConfigService,
    private readonly sessionsService: SessionsService,
    private readonly rolesService: RolesService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromUrlQueryParameter('token'),
        ExtractJwt.fromBodyField('token'),
      ]),
      secretOrKey: configService.get<string>('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: JwtPayload): Promise<any> {
    const { sub, type } = payload;

    const sessionExists = await this.sessionsService.findOneBySubAndType(
      sub,
      type,
    );

    const userRoles = await this.rolesService.findOne(sub);

    if (!sessionExists) {
      throw new UnauthorizedException(
        'Invalid or deleted token. Login to get new token',
      );
    }

    const permissions = userRoles?.permissions;

    return { sessionExists, permissions };
  }
}
