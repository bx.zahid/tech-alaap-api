import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy } from 'passport-facebook';

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
  constructor(configService: ConfigService) {
    super({
      clientID: configService.get<string>('FACEBOOK_APP_ID'),
      clientSecret: configService.get<string>('FACEBOOK_APP_SECRET'),
      callbackURL: configService.get<string>('FACEBOOK_CALLBACK_URL'),
      scope: ['public_profile', 'email'],
      profileFields: [
        'id',
        'birthday',
        'email',
        'first_name',
        'last_name',
        'gender',
        'photos',
      ],
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    cb: (err: any, user: any, info?: any) => void,
  ): Promise<void> {
    const {
      _json: {
        id,
        email,
        first_name,
        last_name,
        picture: {
          data: { url },
        },
      },
    } = profile;

    const payload = {
      facebookSchemaId: id,
      email: email,
      firstName: first_name,
      lastName: last_name,
      profilePhoto: url,
    };

    cb(null, payload);
  }
}
