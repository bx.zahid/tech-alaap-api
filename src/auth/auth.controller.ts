import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Req,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { Admin } from '../admins/schemas/admin.schema';
import { Auth } from '../common/decorators/auth.decorator';
import { AuthType, Permissions } from '../common/enum';
import {
  FacebookSessionRequest,
  GithubSessionRequest,
  GoogleSessionRequest,
  SessionRequest,
} from '../common/interfaces/session.interface';
import { Session } from '../sessions/schemas/session.schema';
import { User } from '../users/schemas/user.schema';
import { Permission } from './../common/decorators/permissions.decorator';
import { AuthService } from './auth.service';
import { CreateLoginDto } from './dto/create-login.dto';
import { CreateSignupDto } from './dto/create-signup.dto';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('user-signup')
  registerUserByEmailAndPassword(
    @Body() createSignupDto: CreateSignupDto,
  ): Promise<User> {
    return this.authService.registerUserByEmailAndPassword(createSignupDto);
  }

  @Post('user-login')
  loginUserByEmailOrUsernameAndPassword(
    @Body() createLoginDto: CreateLoginDto,
  ): Promise<Session> {
    return this.authService.loginUserByEmailOrUsernameAndPassword(
      createLoginDto,
    );
  }

  @Post('user-logout')
  @Auth()
  @Permission(Permissions.LOGOUT)
  logoutUser(@Request() req: SessionRequest): Promise<string> {
    return this.authService.logoutUser(req);
  }

  @Post('admin-signup')
  registerAdminByEmailAndPassword(
    @Body() createSignupDto: CreateSignupDto,
  ): Promise<Admin> {
    return this.authService.registerAdminByEmailAndPassword(createSignupDto);
  }

  @Post('admin-login')
  loginAdminByEmailOrUsernameAndPassword(
    @Body() createLoginDto: CreateLoginDto,
  ): Promise<Session> {
    return this.authService.loginAdminByEmailOrUsernameAndPassword(
      createLoginDto,
    );
  }

  @Post('admin-logout')
  @Auth(AuthType.ADMIN)
  logoutAdmin(@Request() req: SessionRequest): Promise<string> {
    return this.authService.logoutAdmin(req);
  }

  @Get('login-by-google')
  @UseGuards(AuthGuard('google'))
  async googleLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  loginByGoogle(@Req() req: GoogleSessionRequest): Promise<Session | string> {
    return this.authService.loginByGoogle(req);
  }

  @Get('login-by-facebook')
  @UseGuards(AuthGuard('facebook'))
  async facebookLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('facebook/callback')
  @UseGuards(AuthGuard('facebook'))
  async loginByFacebook(
    @Req() req: FacebookSessionRequest,
  ): Promise<Session | string> {
    return this.authService.loginByFacebook(req);
  }

  @Get('login-by-github')
  @UseGuards(AuthGuard('github'))
  async githubLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('github/callback')
  @UseGuards(AuthGuard('github'))
  async loginByGithub(
    @Req() req: GithubSessionRequest,
  ): Promise<Session | string> {
    return this.authService.loginByGithub(req);
  }

  @Post('forgot-password')
  forgotPassword(
    @Req() req: Request,
    @Body() forgotPasswordDto: ForgotPasswordDto,
  ): Promise<string> {
    return this.authService.forgotPassword(req, forgotPasswordDto);
  }

  @Post('reset-password/:token')
  resetPassword(
    @Req() req: Request,
    @Param('token') resetPasswordToken: string,
    @Body() resetPasswordDto: ResetPasswordDto,
  ): Promise<any> {
    return this.authService.resetPassword(
      req,
      resetPasswordToken,
      resetPasswordDto,
    );
  }
}
