import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Matches } from 'class-validator';

export class CreateSignupDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Matches(/^(?=.{3,15}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/, {
    message:
      'Username is 3-15 characters long, No _ or . at the beginning, No __ or _. or ._ or .. inside, No _ or . at the end, Allowed characters: a-z A-Z 0-9',
  })
  username: string;

  @ApiProperty()
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/, {
    message:
      'Password must contain at least 1 lowercase, uppercase, numeric character and 6 character long',
  })
  password: string;
}
