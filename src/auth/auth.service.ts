import {
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ReturnModelType } from '@typegoose/typegoose';
import { hashSync } from 'bcryptjs';
import { InjectModel } from 'nestjs-typegoose';
import { AdminsService } from '../admins/admins.service';
import { Admin } from '../admins/schemas/admin.schema';
import { AuthType } from '../common/enum';
import {
  FacebookSessionRequest,
  GithubSessionRequest,
  GoogleSessionRequest,
  SessionRequest,
} from '../common/interfaces/session.interface';
import { EmailService } from '../email/email.service';
import { Session } from '../sessions/schemas/session.schema';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { User } from '../users/schemas/user.schema';
import { SessionsService } from '../sessions/sessions.service';
import { UsersService } from '../users/users.service';
import { CreateLoginDto } from './dto/create-login.dto';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';

const config: ConfigService = new ConfigService();

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User)
    private readonly userModel: ReturnModelType<typeof User>,
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
    private readonly adminsService: AdminsService,
    private readonly sessionsService: SessionsService,
    private readonly emailService: EmailService,
  ) {}

  async registerUserByEmailAndPassword(
    createUserDto: CreateUserDto,
  ): Promise<User> {
    const users: User = await this.usersService.create(createUserDto);

    await this.emailService.sendSignupMail();

    return users;
  }

  async loginUserByEmailOrUsernameAndPassword(
    createLoginDto: CreateLoginDto,
  ): Promise<Session> {
    const { identifier, password } = createLoginDto;

    const user: User = await this.usersService.findOneByIdentifier(identifier);

    if (!user) throw new UnauthorizedException('Invalid Credentials');

    const matched: boolean = user.comparePassword(password);

    if (!matched) throw new UnauthorizedException('Invalid Credentials');

    const session: Promise<Session> = this.sessionsService.findOneOrCreate(
      user._id,
      AuthType.USER,
    );

    return session;
  }

  async logoutUser(req: SessionRequest): Promise<string> {
    if (this.sessionsService.remove(req)) {
      return 'You have logged out successfully';
    } else {
      throw new ForbiddenException(
        'Invalid token or you have already been logged out',
      );
    }
  }

  async registerAdminByEmailAndPassword(
    createUserDto: CreateUserDto,
  ): Promise<Admin> {
    const count: number = await this.adminsService.count();
    if (count)
      throw new ForbiddenException('Admin registration has been truned off');

    return await this.adminsService.create({ ...createUserDto });
  }

  async loginAdminByEmailOrUsernameAndPassword(
    createLoginDto: CreateLoginDto,
  ): Promise<Session> {
    const { identifier, password } = createLoginDto;

    const admin: Admin = await this.adminsService.findOneByIdentifier(
      identifier,
    );

    if (!admin) throw new UnauthorizedException('Invalid Credentials');

    const matched: boolean = admin.comparePassword(password);

    if (!matched) throw new UnauthorizedException('Invalid Credentials');

    const session: Promise<Session> = this.sessionsService.findOneOrCreate(
      admin._id,
      AuthType.ADMIN,
    );

    return session;
  }

  async logoutAdmin(req: SessionRequest): Promise<string> {
    if (this.sessionsService.remove(req)) {
      return 'You have logged out successfully';
    } else {
      throw new ForbiddenException(
        'Invalid token or you have already been logged out',
      );
    }
  }

  async loginByGoogle(req: GoogleSessionRequest): Promise<Session | string> {
    if (!req.user) {
      return 'No user from google';
    }

    const userExits: User = await this.usersService.findOneByGoogleSchemaId(
      req?.user?.googleSchemaId,
    );

    if (!userExits) {
      const user: User = await this.usersService.createGoogleUser(req?.user);
      return this.sessionsService.findOneOrCreate(user._id, AuthType.USER);
    }

    return this.sessionsService.findOneOrCreate(userExits._id, AuthType.USER);
  }

  async loginByFacebook(
    req: FacebookSessionRequest,
  ): Promise<Session | string> {
    if (!req.user) {
      return 'No user from facebook';
    }

    const userExits: User = await this.usersService.findOneByFacebookSchemaId(
      req?.user?.facebookSchemaId,
    );

    if (!userExits) {
      const user: User = await this.usersService.createFacebookUser(req?.user);
      return this.sessionsService.findOneOrCreate(user._id, AuthType.USER);
    }

    return this.sessionsService.findOneOrCreate(userExits._id, AuthType.USER);
  }

  async loginByGithub(req: GithubSessionRequest): Promise<Session | string> {
    if (!req.user) {
      return 'No user from github';
    }

    const userExits: User = await this.usersService.findOneByGithubSchemaId(
      req?.user?.githubSchemaId,
    );

    if (!userExits) {
      const user: User = await this.usersService.createGithubUser(req?.user);
      return this.sessionsService.findOneOrCreate(user._id, AuthType.USER);
    }

    return this.sessionsService.findOneOrCreate(userExits._id, AuthType.USER);
  }

  async forgotPassword(
    req: Request,
    forgotPasswordDto: ForgotPasswordDto,
  ): Promise<any> {
    const currentUser: User = await this.usersService.findOneByEmail(
      forgotPasswordDto.email,
    );

    if (!currentUser)
      throw new NotFoundException('There is no user with email address');

    const reqObj: any = req.headers;

    const passwordResetToken: string = await this.tokenGenerator(
      currentUser._id,
      {
        expiresIn: '2h',
      },
    );

    await this.userModel.findOneAndUpdate(
      { _id: currentUser._id },
      { passwordResetToken },
      { new: true },
    );

    const resetURL = `${config.get('PROTOCOL')}://${
      reqObj.host
    }/api/${config.get(
      'API_VERSION',
    )}/auth/reset-password/${passwordResetToken}`;

    const template = `<a target="_blank" href="${resetURL}">Reset your password</a>`;

    const isEmailSent: boolean = await this.emailService.sendPasswordResetMail(
      template,
    );

    if (isEmailSent) {
      return {
        message: 'Password reset link has been sent to your email',
      };
    } else {
      await this.userModel.findOneAndUpdate(
        { _id: currentUser._id },
        { passwordResetToken: undefined },
        { new: true },
      );

      throw new ForbiddenException(
        'There was an error sending the email. Try again later!',
      );
    }
  }

  async resetPassword(
    req: Request,
    resetPasswordToken: string,
    resetPasswordDto: ResetPasswordDto,
  ): Promise<any> {
    const { isInvalid, isExpired } = await this.verifyToken(
      resetPasswordToken,
      config.get('JWT_SECRET'),
    );

    if (isInvalid) throw new ForbiddenException('Invalid password reset link');

    const {
      payload: { payload },
    }: any = await this.jwtService.decode(resetPasswordToken, {
      complete: true,
    });

    const user: User = await this.usersService.findOne(payload);

    if (!user) throw new NotFoundException('User not found');

    if (user.passwordResetToken !== resetPasswordToken || isInvalid)
      throw new ForbiddenException('Invalid password reset link');

    if (isExpired) {
      const newPasswordResetToken = await this.tokenGenerator(user._id, {
        expiresIn: '2h',
      });

      await this.userModel.findOneAndUpdate(
        { _id: user._id },
        { passwordResetToken: newPasswordResetToken },
        { new: true },
      );

      const reqObj: any = req.headers;

      const resetURL = `${config.get('PROTOCOL')}://${
        reqObj.host
      }/api/${config.get(
        'API_VERSION',
      )}/auth/reset-password/${newPasswordResetToken}`;

      const template = `<a target="_blank" href="${resetURL}">Reset your password</a>`;

      const isEmailSent: boolean = await this.emailService.sendPasswordResetMail(
        template,
      );

      if (isEmailSent) {
        return {
          message:
            'Password reset link expired. A new password reset link has been sent to your email',
        };
      } else {
        await this.userModel.findOneAndUpdate(
          { _id: user._id },
          { passwordResetToken: undefined },
          { new: true },
        );

        throw new ForbiddenException(
          'There was an error sending the email. Try again later!',
        );
      }
    }

    const hashedPassword: string = hashSync(resetPasswordDto.password, 11);

    await this.userModel.findOneAndUpdate(
      { _id: user._id },
      { password: hashedPassword, passwordResetToken: undefined },
      { new: true },
    );

    return {
      message: 'Password reset has been successfull',
    };
  }

  async tokenGenerator(payload, jwtSignOptions) {
    return await this.jwtService.signAsync({ payload }, jwtSignOptions);
  }

  async verifyToken(token, secret) {
    try {
      return await this.jwtService.verifyAsync(token, secret);
    } catch (error) {
      if (error.message === 'invalid token') {
        return { isInvalid: true };
      }

      if (error.message === 'jwt expired') {
        return { isExpired: true };
      }
    }
  }
}
