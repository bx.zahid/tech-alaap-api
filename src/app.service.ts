import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { Admin } from 'src/admins/schemas/admin.schema';
import { Profile } from 'src/profiles/schemas/profile.schema';
import { Role } from 'src/roles/schemas/roles.schema';
import { Session } from 'src/sessions/schemas/session.schema';
import { User } from 'src/users/schemas/user.schema';
import { Article } from './articles/schemas/articles.schema';

@Injectable()
export class AppService {
  constructor(
    @InjectModel(User)
    private readonly userModel: ReturnModelType<typeof User>,

    @InjectModel(Profile)
    private readonly profileModel: ReturnModelType<typeof Profile>,

    @InjectModel(Article)
    private readonly articleModel: ReturnModelType<typeof Article>,

    @InjectModel(Admin)
    private readonly adminModel: ReturnModelType<typeof Admin>,

    @InjectModel(Role)
    private readonly roleModel: ReturnModelType<typeof Role>,

    @InjectModel(Session)
    private readonly sessionModel: ReturnModelType<typeof Session>,
  ) {}

  getHello(): string {
    return 'Tech Alaap is a place for programmers / tech explorers to share ideas and help each other grow. All developers are welcome to submit stories, tutorials, or anything worth discussing on https://techalaap.com';
  }

  async deleteAllData(): Promise<any> {
    await this.userModel.deleteMany({});
    await this.profileModel.deleteMany({});
    await this.articleModel.deleteMany({});
    await this.adminModel.deleteMany({});
    await this.roleModel.deleteMany({});
    await this.sessionModel.deleteMany({});
    return { message: 'All Data successfully deleted' };
  }
}
