import { ModelOptions, plugin, pre, prop } from '@typegoose/typegoose';
import { compareSync, hashSync } from 'bcryptjs';
import { Types } from 'mongoose';
import * as uniqueValidator from 'mongoose-unique-validator';
import { AuthType } from '../../common/enum';

@plugin(uniqueValidator, { message: '{VALUE} already taken' })
@ModelOptions({ schemaOptions: { timestamps: true } })
@pre<Admin>('save', function () {
  this.password = hashSync(this.password);
})
export class Admin {
  public _id?: Types.ObjectId;

  @prop({ trim: true, unique: true })
  public username?: string;

  @prop({ trim: true, lowercase: true, unique: true })
  public email: string;

  @prop({ trim: true })
  public password?: string;

  @prop({ trim: true, default: '' })
  public firstName?: string;

  @prop({ trim: true, default: '' })
  public lastName?: string;

  @prop({ trim: true, default: '' })
  public profilePhoto?: string;

  @prop({ default: AuthType.ADMIN })
  public authType?: AuthType;

  @prop()
  public passwordChangedAt?: string;

  @prop()
  public passwordResetToken?: string;

  @prop({ default: false })
  public isVerified?: boolean;

  public comparePassword(currentPassword: string): boolean {
    return compareSync(currentPassword, this.password);
  }
}
