import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { SessionsModule } from '../sessions/sessions.module';
import { AdminsController } from './admins.controller';
import { AdminsService } from './admins.service';
import { Admin } from './schemas/admin.schema';

@Module({
  imports: [TypegooseModule.forFeature([Admin]), SessionsModule],
  providers: [AdminsService],
  controllers: [AdminsController],
  exports: [AdminsService],
})
export class AdminsModule {}
