import { Injectable, NotFoundException } from '@nestjs/common';
import { DocumentType, ReturnModelType } from '@typegoose/typegoose';
import { hashSync } from 'bcryptjs';
import { Types } from 'mongoose';
import { InjectModel } from 'nestjs-typegoose';
import { UpdateUserPasswordDto } from '../users/dto/update-user-password.dto';
import { CreateAdminDto } from './dto/create-admin.dto';
import { CreateNewAdminDto } from './dto/create-new-admin.dto';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { RemovePermissionDto } from './dto/remove.permissions.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './schemas/admin.schema';

@Injectable()
export class AdminsService {
  constructor(
    @InjectModel(Admin)
    private readonly adminModel: ReturnModelType<typeof Admin>,
  ) {}

  async create(createAdminDto: CreateAdminDto): Promise<Admin> {
    return await this.adminModel.create(createAdminDto);
  }

  async createNewAdmin(createNewAdminDto: CreateNewAdminDto): Promise<Admin> {
    return await this.adminModel.create(createNewAdminDto);
  }

  async createPermission(
    _id: Types.ObjectId,
    createPermissionDto: CreatePermissionDto,
  ): Promise<Admin> {
    return await this.adminModel.updateOne(
      { _id },
      {
        $addToSet: {
          'role.permissions': createPermissionDto.permissions,
        },
      },
    );
  }

  async findAll(): Promise<DocumentType<Admin>[]> {
    return await this.adminModel.find();
  }

  async findOne(_id: Types.ObjectId): Promise<Admin> {
    return await this.adminModel.findById(_id);
  }

  async findOneByIdentifier(identifier: string): Promise<Admin> {
    return await this.adminModel.findOne({
      $or: [{ username: identifier }, { email: identifier }],
    });
  }

  async update(
    _id: Types.ObjectId,
    updateAdminDto: UpdateAdminDto,
  ): Promise<DocumentType<Admin>> {
    const adminExits: Admin = await this.findOne(_id);

    if (!adminExits)
      throw new NotFoundException('Admin id is not found or invalid');

    return await this.adminModel.findOneAndUpdate(
      { _id },
      { ...updateAdminDto },
      { new: true },
    );
  }

  async updatePassword(
    _id: Types.ObjectId,
    updateUserPasswordDto: UpdateUserPasswordDto,
  ): Promise<string> {
    const adminExits: Admin = await this.findOne(_id);

    const matched: boolean = adminExits.comparePassword(
      updateUserPasswordDto.currentPassword,
    );

    if (!matched) throw new NotFoundException('Your current password is wrong');

    const newHashPassword: string = hashSync(updateUserPasswordDto.newPassword);

    const updated: Admin = await this.adminModel.updateOne(
      { _id },
      { password: newHashPassword },
    );
    if (updated) return 'Password change successfully';
  }

  async remove(_id: Types.ObjectId): Promise<Admin> {
    return await this.adminModel.findByIdAndDelete(_id);
  }

  async removePermission(
    _id: Types.ObjectId,
    removePermissionDto: RemovePermissionDto,
  ): Promise<Admin> {
    return await this.adminModel.updateMany(
      { _id },
      {
        $pull: {
          'role.permissions': { $in: removePermissionDto.permissions },
        },
      },
      { multi: true },
    );
  }

  async count(): Promise<number> {
    return await this.adminModel.countDocuments();
  }
}
