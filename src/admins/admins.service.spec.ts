import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken, TypegooseModule } from 'nestjs-typegoose';
import { UpdateUserPasswordDto } from 'src/users/dto/update-user-password.dto';
import { AdminsService } from './admins.service';
import { CreateAdminDto } from './dto/create-admin.dto';
import { CreateNewAdminDto } from './dto/create-new-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './schemas/admin.schema';

const config: ConfigService = new ConfigService();

describe('AdminsService', () => {
  let adminModel;
  let adminsService: AdminsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypegooseModule.forFeature([Admin]),
        ConfigModule.forRoot(),
        TypegooseModule.forRoot(config.get<string>('DATABASE_URL'), {
          useCreateIndex: true,
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
        }),
      ],
      providers: [AdminsService],
    }).compile();

    adminModel = module.get(getModelToken('Admin'));
    adminsService = module.get<AdminsService>(AdminsService);

    await adminModel.deleteMany({});
  });

  afterEach(async () => {
    await adminModel.deleteMany({});
  });

  it('should be defined', () => {
    expect(adminsService).toBeDefined();
  });

  describe('create', () => {
    it('should be create a admin and return that admin', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id, username, email, password } = await adminsService.create(
        createAdminDto,
      );

      expect(_id).toBeDefined();
      expect(username).toBe(createAdminDto.username);
      expect(email).toBe(createAdminDto.email);
      expect(password).toBeDefined();
    });

    it('should throw error when username or email is not unique', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await adminsService.create(createAdminDto);

      try {
        await adminsService.create(createAdminDto);
      } catch (error) {
        expect(error.name).toBe('ValidationError');
      }
    });
  });

  describe('createNewAdmin', () => {
    it('should be create an admin and return that admin', async () => {
      const createNewAdminDto: CreateNewAdminDto = {
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id, email, password } = await adminsService.createNewAdmin(
        createNewAdminDto,
      );

      expect(_id).toBeDefined();
      expect(email).toBe(createNewAdminDto.email);
      expect(password).toBeDefined();
    });

    it('should throw error when email is not unique', async () => {
      const createNewAdminDto: CreateNewAdminDto = {
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await adminsService.createNewAdmin(createNewAdminDto);

      try {
        await adminsService.createNewAdmin(createNewAdminDto);
      } catch (error) {
        expect(error.name).toBe('ValidationError');
      }
    });
  });

  describe('createPermission', () => {
    it('should be push permission in role and return an array', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await adminsService.create(createAdminDto);

      const createPermissionDto: any = [
        'UPDATE',
        'UPDATE_PROFILE',
        'CREATE_PASSWORD',
        'UPDATE_PASSWORD',
        'FORGOT_PASSWORD',
        'LOGOUT',
      ];

      const response = await adminsService.createPermission(
        _id,
        createPermissionDto,
      );

      expect(response).toBeDefined();
    });
  });

  // FIXME:
  // describe('findAll', () => {
  //   it('should be return an array of admin', async () => {
  //     const createAdminDto: CreateAdminDto = {
  //       username: 'bxzahid',
  //       email: 'zahidhasan.dev@gmail.com',
  //       password: 'Zahid123456',
  //     };

  //     await adminsService.create(createAdminDto);

  //     const fetchedAdmin: DocumentType<Admin>[] = await adminsService.findAll();

  //     expect(fetchedAdmin._id).toBeDefined();
  //     expect(fetchedAdmin.username).toBe(createAdminDto.username);
  //     expect(fetchedAdmin.email).toBe(createAdminDto.email);
  //     expect(fetchedAdmin.password).toBeDefined();
  //     expect(fetchedAdmin.authType).toEqual('ADMIN');
  //   });
  // });

  describe('findOne', () => {
    it('should be return an admin by _id', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await adminsService.create(createAdminDto);

      const {
        _id: id,
        username,
        email,
        password,
      } = await adminsService.findOne(_id);

      expect(id).toBeDefined();
      expect(username).toBe(createAdminDto.username);
      expect(email).toBe(createAdminDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('findOneByIdentifier', () => {
    it('should be return an admin by username / email', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await adminsService.create(createAdminDto);

      const {
        _id,
        username,
        email,
        password,
      } = await adminsService.findOneByIdentifier(createAdminDto.username);

      expect(_id).toBeDefined();
      expect(username).toBe(createAdminDto.username);
      expect(email).toBe(createAdminDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('update', () => {
    it('should be update admin and return that updated admin', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await adminsService.create(createAdminDto);

      const updateAdminDto: UpdateAdminDto = {
        username: 'updated username',
        email: 'updated.dev@gmail.com',
        password: 'Updated123456',
        firstName: 'Updated firstName',
        lastName: 'Updated lastName',
        profilePhoto: 'this-is-a-updated-profile-photo',
      };

      const {
        _id: id,
        username,
        email,
        password,
        firstName,
        lastName,
        profilePhoto,
      } = await adminsService.update(_id, updateAdminDto);

      expect(id).toBeDefined();
      expect(username).toBe(updateAdminDto.username);
      expect(email).toBe(updateAdminDto.email);
      expect(firstName).toBe(updateAdminDto.firstName);
      expect(lastName).toBe(updateAdminDto.lastName);
      expect(profilePhoto).toBe(updateAdminDto.profilePhoto);
      expect(password).toBeDefined();
    });
  });

  describe('updatePassword', () => {
    it('should be update admin password and return a string', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await adminsService.create(createAdminDto);

      const updateUserPasswordDto: UpdateUserPasswordDto = {
        currentPassword: 'Zahid123456',
        newPassword: 'Updated123456',
      };

      const response = await adminsService.updatePassword(
        _id,
        updateUserPasswordDto,
      );

      expect('Password change successfully').toBe(response);
    });
  });

  describe('remove', () => {
    it('should be remove admin by _id and return that removed admin', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await adminsService.create(createAdminDto);

      const { _id: id, username, email, password } = await adminsService.remove(
        _id,
      );

      expect(id).toBeDefined();
      expect(username).toBe(createAdminDto.username);
      expect(email).toBe(createAdminDto.email);
      expect(password).toBeDefined();
    });
  });

  describe('removePermission', () => {
    it('should be remove permission from role permissions', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      const { _id } = await adminsService.create(createAdminDto);

      const createPermissionDto: any = [
        'UPDATE',
        'UPDATE_PROFILE',
        'CREATE_PASSWORD',
        'UPDATE_PASSWORD',
        'FORGOT_PASSWORD',
        'LOGOUT',
      ];

      await adminsService.createPermission(_id, createPermissionDto);

      const removePermissionDto: any = [
        'UPDATE',
        'UPDATE_PROFILE',
        'CREATE_PASSWORD',
      ];

      const response = await adminsService.removePermission(
        _id,
        removePermissionDto,
      );

      expect(response).toBeDefined();
    });
  });

  describe('count', () => {
    it('should be return total number of admin', async () => {
      const createAdminDto: CreateAdminDto = {
        username: 'bxzahid',
        email: 'zahidhasan.dev@gmail.com',
        password: 'Zahid123456',
      };

      await adminsService.create(createAdminDto);

      const response = await adminsService.count();

      expect(response).toBe(1);
    });
  });
});
