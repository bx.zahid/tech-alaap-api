import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DocumentType } from '@typegoose/typegoose';
import { Types } from 'mongoose';
import { AuthType } from '../common/enum';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UpdateUserPasswordDto } from '../users/dto/update-user-password.dto';
import { Auth } from '../common/decorators/auth.decorator';
import { AdminsService } from './admins.service';
import { CreateNewAdminDto } from './dto/create-new-admin.dto';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { RemovePermissionDto } from './dto/remove.permissions.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './schemas/admin.schema';

@ApiTags('Admin')
@Controller('admins')
export class AdminsController {
  constructor(private readonly adminsService: AdminsService) {}

  @Post()
  @Auth(AuthType.ADMIN)
  create(@Body() createUserDto: CreateUserDto): Promise<Admin> {
    return this.adminsService.create(createUserDto);
  }

  @Post('create-new-admin')
  @Auth(AuthType.ADMIN)
  createNewAdmin(@Body() createNewAdminDto: CreateNewAdminDto): Promise<Admin> {
    return this.adminsService.createNewAdmin(createNewAdminDto);
  }

  @Post('create-permission/:id')
  @Auth(AuthType.ADMIN)
  createPermission(
    @Param('id') _id: Types.ObjectId,
    @Body() createPermissionDto: CreatePermissionDto,
  ): Promise<Admin> {
    return this.adminsService.createPermission(_id, createPermissionDto);
  }

  @Get()
  @Auth(AuthType.ADMIN)
  findAll(): Promise<DocumentType<Admin>[]> {
    return this.adminsService.findAll();
  }

  @Get(':id')
  @Auth(AuthType.ADMIN)
  findOne(@Param('id') _id: Types.ObjectId): Promise<Admin> {
    return this.adminsService.findOne(_id);
  }

  // TODO: Have to check it.
  @Patch(':id')
  @Auth(AuthType.ADMIN)
  update(
    @Param('id') _id: Types.ObjectId,
    @Body() updateAdminDto: UpdateAdminDto,
  ): Promise<DocumentType<Admin>> {
    return this.adminsService.update(_id, updateAdminDto);
  }

  @Patch('update-password/:id')
  @Auth(AuthType.ADMIN)
  updatePassword(
    @Param('id') _id: Types.ObjectId,
    @Body() updateUserPasswordDto: UpdateUserPasswordDto,
  ): Promise<string> {
    return this.adminsService.updatePassword(_id, updateUserPasswordDto);
  }

  @Delete(':id')
  @Auth(AuthType.ADMIN)
  remove(@Param('id') _id: Types.ObjectId): Promise<Admin> {
    return this.adminsService.remove(_id);
  }

  @Delete('remove-permission/:id')
  @Auth(AuthType.ADMIN)
  removePermission(
    @Param('id') _id: Types.ObjectId,
    @Body() removePermissionDto: RemovePermissionDto,
  ): Promise<Admin> {
    return this.adminsService.removePermission(_id, removePermissionDto);
  }
}
